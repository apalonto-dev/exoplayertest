package com.madbrains.myapplication

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.FragmentTransaction

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        replaceToMainFragment()
    }

    fun replaceToExoPlayerFragment() {
        val fragmentExoPlayer = FragmentExoPlayer()
        val ft: FragmentTransaction = supportFragmentManager.beginTransaction()
        ft.replace(R.id.container, fragmentExoPlayer, "player")
        ft.commit()
    }

    fun replaceToMainFragment() {
        val mainFragment = FragmentMain()
        val ft: FragmentTransaction = supportFragmentManager.beginTransaction()
        ft.replace(R.id.container, mainFragment, "main")
        ft.commit()
    }

}
